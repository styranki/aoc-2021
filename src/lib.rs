#![feature(type_alias_impl_trait, iter_advance_by, associated_type_defaults, box_patterns)]
#![warn(clippy::all, clippy::pedantic, clippy::nursery)]
#![allow(clippy::wildcard_imports, clippy::default_trait_access)]

pub mod days;
pub mod solver;
pub(crate) mod util;
