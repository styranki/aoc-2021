use crate::{solver::Solver, util::*};
use std::collections::HashSet;

pub struct Day20;

impl<'a> Solver<'a> for Day20 {
    type Parsed = (Vec<u8>, Vec<Vec<i32>>);
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        let (algorithm, image) = input.split_once("\n\n").unwrap();

        let mut out = Vec::new();
        for line in image.lines() {
            let v: Vec<i32> = line.chars().map(|c| if c == '#' { 1 } else { 0 }).collect();
            out.push(v);
        }

        (
            algorithm.chars().map(|c| if c == '#' { 1 } else { 0 }).collect(),
            out
        )
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let (algorithm, mut image) = data;
        //dbg!(algorithm.len());
        //dbg!(image[0].len());
        //dbg!(image.len());

        //display_image(&image);
        for count in 0..2 {
            let width = image[0].len();
            let height = image.len();
            let mut new_img = Vec::new();
            for j in 0..height+2 {
                let mut row = Vec::new();
                for i in 0..width+2 {
                    row.push(algorithm[
                        sample_image(&image, (i as i32-1 , j as i32-1), (count) % 2) as usize
                    ] as i32);
                }
                new_img.push(row);
            }
            image = new_img;

            //display_image(&image);
        }

        image.iter().fold(0, |acc, row| {
            acc + row.iter().filter(|&&x| x == 1).count() as u32
        })
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let (algorithm, mut image) = data;

        for count in 0..50 {
            let width = image[0].len();
            let height = image.len();
            let mut new_img = Vec::new();
            for j in 0..height+2 {
                let mut row = Vec::new();
                for i in 0..width+2 {
                    row.push(algorithm[
                        sample_image(&image, (i as i32-1 , j as i32-1), (count) % 2) as usize
                    ] as i32);
                }
                new_img.push(row);
            }
            image = new_img;

            //display_image(&image);
        }

        image.iter().fold(0, |acc, row| {
            acc + row.iter().filter(|&&x| x == 1).count() as u32
        })
    }
}

fn sample_image(img: &Vec<Vec<i32>>, point: (i32, i32), fill: i32) -> i32 {
    let mut out = 0;
    let mut count: i32 = 8;

    let width = img[0].len();
    let height = img.len();
    let mut dbg_img: Vec<Vec<i32>> = Vec::new();
    for j in (point.1 - 1)..=(point.1 + 1) {
        let mut row = Vec::new();
            for i in (point.0 - 1)..=(point.0 + 1) {
            if i >= width as i32 || i < 0 || j >= height as i32 || j < 0  {
                out += 2_i32.pow((count) as u32) * fill;
                row.push(fill);
            } else {
                row.push(img[j as usize][i as usize]);
                out += 2_i32.pow((count) as u32) * img[j as usize][i as usize] as i32;
            }
            count -= 1;

        }
        dbg_img.push(row);
    }
    /*
    display_image(&dbg_img);
    dbg!(out);
    if point == (2,2) {

    }
    */
    out
}
fn display_image(img: &Vec<Vec<i32>>) {
    println!("### IMAGE ###");
    for row in img {
        let row_str: String = row.iter().map(|&x| if x == 1 { '#' } else { '.'}).collect();
        println!("{}",row_str);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d20p1() {
        assert_eq!(Day20::part1(Day20::parse("..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###")), 35);
    }

    #[test]
    fn d20p2() {
        assert_eq!(Day20::part2(Day20::parse("")), 0);
    }
}
