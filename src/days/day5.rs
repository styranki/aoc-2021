use crate::{solver::Solver, util::*};

pub struct Day5;

#[derive(Clone, Copy, Debug)]
pub struct Line {
    start: (i32, i32),
    end: (i32, i32),
}

fn read_pair_u32(str: &str) -> (i32, i32) {
    let mut tmp = str.split(",");
    (
        tmp.next().unwrap().parse::<i32>().unwrap(),
        tmp.next().unwrap().parse::<i32>().unwrap(),
    )
}
fn gen_line_points(line: &Line) -> Vec<(i32, i32)> {
    let x_range = (line.end.0 - line.start.0);
    let y_range = (line.end.1 - line.start.1);

    let line_length = x_range.abs().max(y_range.abs());

    let mut out = Vec::new();
    for i in 0..=line_length {
        out.push((
            line.start.0 + (i * x_range.signum()),
            (line.start.1 + (i * y_range.signum())),
        ));
    }
    out
}

impl<'a> Solver<'a> for Day5 {
    type Parsed = Vec<Line>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|l| {
                let mut s = l.split(" -> ");
                let mut start = read_pair_u32(&s.next().unwrap());
                let mut end = read_pair_u32(&s.next().unwrap());
                Line { start, end }
            })
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut area = vec![vec![0u32; 1000]; 1000];
        for line in data.iter() {
            let d_x = (line.start.0 - line.end.0).abs();
            let d_y = (line.start.1 - line.end.1).abs();
            // Filter diagonal lines
            if (d_x > 0 && d_y > 0) {
                continue;
            }

            let points_in_line = gen_line_points(line);
            for (x, y) in points_in_line.iter() {
                area[*x as usize][*y as usize] = area[*x as usize][*y as usize] + 1;
            }
        }
        area.iter().fold(0, |acc, col| {
            let num = col.iter().filter(|&&x| x > 1).count();
            acc + num as u32
        })
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut area = vec![vec![0u32; 1000]; 1000];
        for line in data.iter() {
            let points_in_line = gen_line_points(line);
            for (x, y) in points_in_line.iter() {
                area[*x as usize][*y as usize] = area[*x as usize][*y as usize] + 1;
            }
        }
        area.iter().fold(0, |acc, col| {
            let num = col.iter().filter(|&&x| x > 1).count();
            acc + num as u32
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d5p1() {
        assert_eq!(
            Day5::part1(Day5::parse(
                "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"
            )),
            5
        );
    }

    #[test]
    fn d5p2() {
        assert_eq!(
            Day5::part2(Day5::parse(
                "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"
            )),
            12
        );
    }
}
