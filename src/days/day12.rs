use crate::{solver::Solver, util::*};
use itertools::Itertools;
use std::collections::HashMap;

pub struct Day12;

impl<'a> Solver<'a> for Day12 {
    type Parsed = Vec<(&'a str, &'a str)>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|line| {
                let tmp: Vec<&str> = line.split("-").collect();
                (tmp[0], tmp[1])
            })
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut adj_list: HashMap<&str, Vec<&str>> = HashMap::new();
        for points in data.iter() {
            let p0 = adj_list.get_mut(points.0);
            if let Some(l0) = p0 {
                l0.push(points.1);
            } else {
                adj_list.insert(points.0, vec![points.1]);
            }

            let p1 = adj_list.get_mut(points.1);
            if let Some(l1) = p1 {
                l1.push(points.0);
            } else {
                adj_list.insert(points.1, vec![points.0]);
            }
        }

        let start = vec!["start"];
        let mut complete_paths = Vec::new();
        explore(&start, &adj_list, &mut complete_paths, false);
        complete_paths.len() as u32
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut adj_list: HashMap<&str, Vec<&str>> = HashMap::new();
        for points in data.iter() {
            let p0 = adj_list.get_mut(points.0);
            if let Some(l0) = p0 {
                l0.push(points.1);
            } else {
                adj_list.insert(points.0, vec![points.1]);
            }

            let p1 = adj_list.get_mut(points.1);
            if let Some(l1) = p1 {
                l1.push(points.0);
            } else {
                adj_list.insert(points.1, vec![points.0]);
            }
        }

        let start = vec!["start"];
        let mut complete_paths = Vec::new();
        explore(&start, &adj_list, &mut complete_paths, true);
        complete_paths.len() as u32
    }
}

fn explore<'a>(
    path: &Vec<&'a str>,
    adj_list: &HashMap<&'a str, Vec<&'a str>>,
    complete_paths: &mut Vec<Vec<&'a str>>,
    part2: bool,
) {
    //dbg!(&path);
    let mut neighbors = adj_list.get(path[path.len() - 1]).unwrap().clone();
    //dbg!(&neighbors);
    let explored_small_caves: Vec<&str> = path
        .iter()
        .filter(|x| &&x.to_lowercase() == x)
        .map(|x| *x)
        .collect();
    let explored_small_counts = explored_small_caves.iter().unique().count();

    //dbg!(&explored_small_caves);
    if !part2 || explored_small_counts < explored_small_caves.len() {
        neighbors = neighbors
            .iter()
            .filter(|&x| explored_small_caves.iter().all(|y| x != y))
            .map(|x| *x)
            .collect();
    }

    //dbg!(&neighbors);
    for v in neighbors.iter() {
        //dbg!(v);
        let mut c_path = path.clone();
        c_path.push(v);
        if *v == "start" {
            continue;
        } else if *v == "end" {
            complete_paths.push(c_path);
        } else {
            explore(&c_path, adj_list, complete_paths, part2);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d12p1() {
        assert_eq!(
            Day12::part1(Day12::parse(
                "start-A
start-b
A-c
A-b
b-d
A-end
b-end"
            )),
            10
        );
    }

    #[test]
    fn d12p2() {
        assert_eq!(Day12::part2(Day12::parse("")), 0);
    }
}
