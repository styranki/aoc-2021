use itertools::Itertools;

use crate::{solver::Solver, util::*};

pub struct Day17;

impl<'a> Solver<'a> for Day17 {
    type Parsed = [i32; 4];
    type Output = i32;

    fn parse(input: &'a str) -> Self::Parsed {
        let (_, bound_str) = input.split_once(": ").unwrap();
        let (mut x_str, mut y_str) = bound_str.split_once(", ").unwrap();
        let x_str = x_str.strip_prefix("x=").unwrap();
        let y_str = y_str.strip_prefix("y=").unwrap();
        //dbg!(&y_str, &x_str);
        let (min_x, max_x) = x_str
            .split("..")
            .into_iter()
            .tuples()
            .map(|x: (&str, &str)| (x.0.parse::<i32>().unwrap(), x.1.parse::<i32>().unwrap()))
            .next()
            .unwrap();
        let (min_y, max_y) = y_str
            .split("..")
            .into_iter()
            .tuples()
            .map(|x: (&str, &str)| (x.0.parse::<i32>().unwrap(), x.1.parse::<i32>().unwrap()))
            .next()
            .unwrap();
        [min_x, min_y, max_x, max_y]
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let x_vel_min = (data[0] as f32 * 2.0).sqrt();
        let y_vel = data[1] * -1 - 1;
        //dbg!(x_vel_min, y_vel);
        let traj: Vec<(i32, i32)> = generate_trajectory((x_vel_min.round() as i32, y_vel))
            //.take_while(|(x, y)| x < &data[0] || x > &data[2] || y < &data[1] || y > &data[3])
            .take_while(|(_, y)| y >= &data[1])
            .collect();
        //dbg!(traj);
        (1 + y_vel) * (y_vel) /2
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let x_vel_min = (data[0] as f32 * 2.0).sqrt().round() as i32;
        let mut res = Vec::new();
        for x_vel in x_vel_min..300 {
            for y_vel in -100..1000 {
                let mut traj = generate_trajectory((x_vel, y_vel))
                    .take_while(|(_, y)| y >= &data[1]);
                if traj.any(|(x,y)| x >= data[0] && x <= data[2] && y >= data[1] && y <= data[3]) {
                    res.push((x_vel, y_vel));
                }

            }
        }
        if res.len() > std::i32::MAX as usize {
            panic!("Overflow");
        }
        res.len() as i32
    }
}
fn generate_trajectory(initial_velocity: (i32, i32)) -> impl Iterator<Item = (i32, i32)> {
    let vel = initial_velocity.clone();
    std::iter::repeat((0, 0)).enumerate().map(move |(i, pos)| {
        let current_x_vel = vel.0 - (i as i32 - 1);
        let x_pos = if current_x_vel < 0 {
            ((vel.0.clone()+1) * vel.0.clone()) / 2
        } else {
            pos.0 + i as i32 * ((vel.0.clone() - (i as i32 - 1)) + vel.0.clone()) / 2
        };
        return (
            x_pos,
            pos.1 + i as i32 * ((vel.1.clone() - (i as i32 - 1)) + vel.1.clone()) / 2,
        );
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d17p1() {
        assert_eq!(
            Day17::part1(Day17::parse("target area: x=20..30, y=-10..-5")),
            45
        );
    }

    #[test]
    fn d17p2() {
        assert_eq!(Day17::part2(Day17::parse("target area: x=20..30, y=-10..-5")), 112);
    }
}
