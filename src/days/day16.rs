use crate::{solver::Solver, util::*};
use bitvec::prelude::*;
use std::boxed::Box;

pub struct Day16;

#[derive(Clone, Debug)]
enum Packet {
    Literal {
        version: u8,
        type_id: u8,
        data: u64,
    },
    Operator {
        version: u8,
        type_id: u8,
        sub_packets: Vec<Packet>,
    },
}

fn parse_packets(bits: BitVec<u8, Msb0>, packets_left: Option<u32>) -> (Vec<Packet>, usize) {
    let mut pos = 0;
    let mut idx = 0;
    let mut res = vec![];
    if bits.len() <= 10 || packets_left == Some(0) {
        return (res, 0);
    }

    let version: u8 = bits[pos..pos + 3].load_be();
    let packet_type: u8 = bits[pos + 3..pos + 6].load_be();

    //dbg!(&bits);
    //dbg!(version, packet_type);
    // If packet is literal
    if packet_type == 4 {
        //println!("Literal Packet");
        let mut bits_idx = 0;
        let mut bits_vec: BitVec<u8, Msb0> = BitVec::new();
        'bits: loop {
            idx = pos + bits_idx + 6;
            let mut packet_bits: BitVec<u8, _> = bits[idx + 1..idx + 5].into();
            bits_vec.append(&mut packet_bits);

            let last_bits = bits[idx..idx + 1].load::<u8>() == 0;
            if !last_bits {
                bits_idx += 5;
            } else {
                break 'bits;
            }
        }
        let num: u64 = bits_vec.load_be();
        //dbg!(bits_vec);
        //dbg!(num);
        pos = idx + 5;
        res.push(Packet::Literal {
            version,
            type_id: packet_type,
            data: num,
        });
    } else {
        //println!("Operator packet");
        let length_type: bool = !(bits[pos + 6..pos + 7].load::<u8>() > 0);

        if length_type {
            //println!("Bit length");
            let length_in_bits: u64 = bits[pos + 7..pos + 7 + 15].load_be();
            let end = pos + 7 + 15 + length_in_bits as usize;
            //dbg!(length_in_bits, &bits[pos + 7..pos + 7 + 15]);
            //dbg!(&bits[pos+7+15..(pos+7+15+length_in_bits as usize)]);
            let (pack, len) = parse_packets(bits[pos + 7 + 15..end].to_bitvec(), None);
            res.push(Packet::Operator {
                version,
                type_id: packet_type,
                sub_packets: pack,
            });
            //dbg!(end, len);
            pos = end;
        } else {
            //println!("Number of packets");
            let number_of_subpackets: u64 = bits[pos + 7..pos + 7 + 11].load_be();
            //dbg!(number_of_subpackets);
            let mut subs = Vec::new();
            for n in (0..number_of_subpackets).rev() {
                let (mut pack, len) = parse_packets(bits[pos + 7 + 11..].to_bitvec(), Some(1));
                pos = pos + len;
                subs.append(&mut pack);
            }
            res.push(Packet::Operator {
                version,
                type_id: packet_type,
                sub_packets: subs,
            });
            pos = pos + 7 + 11;
        }
    }

    //dbg!(&bits);
    //dbg!(&bits[pos..]);
    let (mut pack, len) = parse_packets(bits[pos..].to_bitvec(), packets_left.map(|x| x - 1));
    pos += len;
    res.append(&mut pack);

    return (res, pos);
}
impl<'a> Solver<'a> for Day16 {
    type Parsed = BitVec<u8, Msb0>;
    type Output = u64;

    fn parse(input: &'a str) -> Self::Parsed {
        let bytes = hex::decode(input).unwrap();
        let data_bits: BitVec<u8, Msb0> = BitVec::from_vec(bytes);

        data_bits
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let packets = parse_packets(data, None);
        version_sum(&packets.0[0])
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let packets = parse_packets(data, None);
        eval(&packets.0[0])
    }
}

fn version_sum(p: &Packet) -> u64 {
    match p {
        Packet::Literal { version, .. } => *version as u64,
        Packet::Operator {
            version,
            sub_packets,
            ..
        } => *version as u64 + sub_packets.iter().map(version_sum).sum::<u64>(),
    }
}
fn eval(p: &Packet) -> u64 {
    match p {
        Packet::Literal { data, .. } => *data as u64,
        Packet::Operator {
            version,
            sub_packets,
            type_id,
        } => match type_id {
            0 => sub_packets.iter().fold(0, |acc, val| acc + eval(val)),
            1 => sub_packets.iter().fold(1, |acc, val| acc * eval(val)),
            2 => sub_packets.iter().map(eval).min().unwrap(),
            3 => sub_packets.iter().map(eval).max().unwrap(),
            5 => {
                let mut iter = sub_packets.iter();
                let first = eval(iter.next().unwrap());
                let second = eval(iter.next().unwrap());
                if first > second {
                    1
                } else {
                    0
                }
            }
            6 => {
                let mut iter = sub_packets.iter();
                let first = eval(iter.next().unwrap());
                let second = eval(iter.next().unwrap());
                if first < second {
                    1
                } else {
                    0
                }
            }
            7 => {
                let mut iter = sub_packets.iter();
                let first = eval(iter.next().unwrap());
                let second = eval(iter.next().unwrap());
                if first == second {
                    1
                } else {
                    0
                }
            }
            _ => {
                unreachable!()
            }
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d16p1() {
        assert_eq!(Day16::part1(Day16::parse("D2FE28")), 6);
    }
    #[test]
    fn operator1() {
        assert_eq!(Day16::part1(Day16::parse("38006F45291200")), 9);
    }

    #[test]
    fn operator2() {
        assert_eq!(Day16::part1(Day16::parse("EE00D40C823060")), 7 + 2 + 4 + 1);
    }

    #[test]
    fn operator3() {
        assert_eq!(Day16::part1(Day16::parse("8A004A801A8002F478")), 16);
    }

    #[test]
    fn operator4() {
        assert_eq!(Day16::part1(Day16::parse("620080001611562C8802118E34")), 12);
    }
    #[test]
    fn operator5() {
        assert_eq!(
            Day16::part1(Day16::parse("C0015000016115A2E0802F182340")),
            23
        );
    }
    #[test]
    fn d16p2() {
        assert_eq!(Day16::part2(Day16::parse("")), 0);
    }
}
