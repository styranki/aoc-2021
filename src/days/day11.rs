use crate::{solver::Solver, util::*};

pub struct Day11;

impl<'a> Solver<'a> for Day11 {
    type Parsed = Vec<Vec<i32>>;
    type Output = usize;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|ln| ln.bytes().map(|x| (x - b'0') as i32).collect())
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut map = data;
        let mut val = 0;
        for i in 0..100 {
            val += step(&mut map);
        }
        val
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut map = data;
        let mut val = 0;
        for i in 1.. {
            if step(&mut map) == 100 {
                val = i;
                break;
            }
        }
        val
    }
}

fn step(map: &mut Vec<Vec<i32>>) -> usize {
    let mut res = 0;
    let mut poppers = vec![];
    for i in 0..10 {
        for j in 0..10 {
            map[i][j] += 1;
            if map[i][j] >= 10 {
                poppers.push((i, j));
                res += 1;
            }
        }
    }

    while let Some((i, j)) = poppers.pop() {
        for neigh_i in (i).saturating_sub(1)..(i + 2).min(10) {
            for neigh_j in (j).saturating_sub(1)..(j + 2).min(10) {
                if map[neigh_i][neigh_j] < 10 {
                    map[neigh_i][neigh_j] += 1;
                    if map[neigh_i][neigh_j] == 10 {
                        poppers.push((neigh_i, neigh_j));
                        res += 1;
                    }
                }
            }
        }
    }

    for i in 0..10 {
        for j in 0..10 {
            if map[i][j] == 10 {
                map[i][j] = 0;
            }
        }
    }

    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d11p1() {
        assert_eq!(
            Day11::part1(Day11::parse(
                "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"
            )),
            1656
        );
    }

    #[test]
    fn d11p2() {
        assert_eq!(Day11::part2(Day11::parse("")), 0);
    }
}
