use crate::{solver::Solver, util::*};
use arrayvec::ArrayVec;

pub struct Day3;
pub const DATA_WIDTH: usize = 12;

impl<'a> Solver<'a> for Day3 {
    type Parsed = Vec<ArrayVec<u32, DATA_WIDTH>>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|line| {
                line.chars()
                    .map(|x| match x {
                        '1' => 1,
                        '0' => 0,
                        _ => unreachable!(),
                    })
                    .collect()
            })
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let n = data.len();
        let sum = data
            .iter()
            .fold(std::iter::repeat(0).take(12).collect(), |acc, val| {
                sum(acc, val)
            });
        let res: ArrayVec<u32, DATA_WIDTH> = sum
            .iter()
            .map(|&x| (x as f32 / n as f32).round() as u32)
            .collect();
        let (gamma, epsilon) = res.iter().rev().enumerate().fold((0, 0), |acc, (i, val)| {
            (
                acc.0 + 2u32.pow(i as u32) * val,
                acc.1 + 2u32.pow(i as u32) * (1 - val),
            )
        });
        gamma * epsilon
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let n = data.len();
        let mut dat = data.clone();
        for i in 0..DATA_WIDTH {
            if dat.len() < 2 {
                break;
            }
            let sum = dat
                .iter()
                .fold(std::iter::repeat(0).take(12).collect(), |acc, val| {
                    sum(acc, val)
                });
            let res: ArrayVec<u32, DATA_WIDTH> = sum
                .iter()
                .map(|&x| {
                    if x > dat.len() as u32 / 2 || (dat.len() % 2 == 0 && x == dat.len() as u32 / 2)
                    {
                        1
                    } else {
                        0
                    }
                })
                .collect();
            dat = dat
                .iter()
                .filter(|x| x[i] == res[i])
                .map(|x| x.to_owned())
                .collect::<Vec<ArrayVec<u32, DATA_WIDTH>>>();
        }
        let oxygen = dat[0]
            .iter()
            .rev()
            .enumerate()
            .fold(0, |acc, (i, val)| acc + 2u32.pow(i as u32) * val);

        let mut dat = data.clone();
        for i in 0..DATA_WIDTH {
            if dat.len() < 2 {
                break;
            }
            let sum = dat
                .iter()
                .fold(std::iter::repeat(0).take(12).collect(), |acc, val| {
                    sum(acc, val)
                });
            let res: ArrayVec<u32, DATA_WIDTH> = sum
                .iter()
                .map(|&x| {
                    if x > dat.len() as u32 / 2
                        || (dat.len() % 2 == 0 && x == (dat.len() as u32 / 2))
                    {
                        1
                    } else {
                        0
                    }
                })
                .collect();
            dat = dat
                .iter()
                .filter(|x| x[i] != res[i])
                .map(|x| x.to_owned())
                .collect::<Vec<ArrayVec<u32, DATA_WIDTH>>>();
        }
        let co2 = dat[0]
            .iter()
            .rev()
            .enumerate()
            .fold(0, |acc, (i, val)| acc + 2u32.pow(i as u32) * val);

        co2 * oxygen
    }
}
// Const generics?
fn sum(a: ArrayVec<u32, DATA_WIDTH>, b: &ArrayVec<u32, DATA_WIDTH>) -> ArrayVec<u32, DATA_WIDTH> {
    a.iter().zip(b.iter()).map(|(x, y)| x + y).collect()
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d3p1() {
        assert_eq!(
            Day3::part1(Day3::parse(
                "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"
            )),
            198
        );
    }

    #[test]
    fn d3p2() {
        assert_eq!(
            Day3::part2(Day3::parse(
                "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"
            )),
            230
        );
    }
}
