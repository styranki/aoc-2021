use crate::{solver::Solver, util::*};

pub struct Day6;

impl<'a> Solver<'a> for Day6 {
    type Parsed = Vec<u32>;
    type Output = u64;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .next()
            .unwrap()
            .split(",")
            .map(|x| x.parse::<u32>().unwrap())
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let num_days = 80;
        let mut fish = data.clone();
        for i in 0..num_days {
            let mut num_new = 0;
            fish.iter_mut().for_each(|mut x| {
                if *x == 0 {
                    num_new = num_new + 1;
                    *x = 6
                } else {
                    *x = *x - 1
                }
            });
            let mut new_fish = vec![8; num_new];
            fish.append(&mut new_fish);
        }
        fish.len() as u64
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut fish = [0_u64; 9];
        for f in data {
            fish[f as usize] += 1;
        }

        let num_days = 256;
        for d in 0..num_days {
            fish.rotate_left(1);
            fish[6] += fish[8];
        }

        fish.iter().sum::<u64>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d6p1() {
        assert_eq!(Day6::part1(Day6::parse("3,4,3,1,2")), 5934);
    }

    #[test]
    fn d6p2() {
        assert_eq!(Day6::part2(Day6::parse("3,4,3,1,2")), 26984457539);
    }
}
