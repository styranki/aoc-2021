use crate::{solver::Solver, util::*};
use std::collections::{HashSet, VecDeque};
use itertools::Itertools;

pub struct Day19;
const PI: f32 =  std::f32::consts::PI;

impl<'a> Solver<'a> for Day19 {
    type Parsed = Vec<Vec<[i32; 3]>>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        let scanners = input.split("\n\n");
        let mut res = Vec::new();
        for scanner in scanners {
            let mut points = Vec::new();
            for point_str in scanner.lines().skip(1) {
                let tmp: Vec<i32> = point_str.split(",").map(|x| x.parse::<i32>().unwrap()).collect();
                points.push([tmp[0], tmp[1], tmp[2]]);
            }
            res.push(points);
        }
        res
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        return 0;
        let sort_triple = |a: &[i32; 3], b: &[i32; 3]|{
            a[0].cmp(&b[0])
                .then(a[1].cmp(&b[1]))
                .then(a[2].cmp(&b[2]))
        };
        let mut scanner0 = data[0].to_vec();
        let mut ready_points: HashSet<[i32; 3]> = HashSet::from_iter(scanner0.iter().map(|x| *x));
        let mut connections = Vec::new();
        /*
        use std::collections::VecDeque;

        let mut not_visited = (1..data.len()).collect::<HashSet<_>>();
        let mut queue = VecDeque::from([data[0].clone()]);
        let mut beacons = vec![data[0].clone()];
        let mut scanners = vec![[0, 0, 0]];

        //dbg!(ready_points.len());
        while !queue.is_empty() {
            let next = queue.pop_front().unwrap();
            for i in not_visited.clone().iter() {
                let scanner1 = &data[*i];
                'outer: for s in 0..6 {
                    for rot in 0..6 {
                        let mut transformed: Vec<[i32; 3]> = scanner1.iter().map(|p| gen_rotation(p, s, rot)).collect();

                        for i in 0..transformed.len() {
                            let point2 = transformed[i].clone();
                            for j in 0..next.len() {
                                let point1 = next[j].clone();


                                let target_relative = relative_positions(&transformed, point2);
                                let src_vec: Vec<_> = next.iter().map(|x|*x).collect();
                                let source_relative = relative_positions(&src_vec, point1);

                                let set1: HashSet<[i32; 3]> = HashSet::from_iter(source_relative);
                                let set2: HashSet<[i32; 3]> = HashSet::from_iter(target_relative);
                                let intersection = set1.intersection(&set2);
                                let intersection_set: HashSet<&[i32; 3]> = intersection.collect();
                                if intersection_set.len() >= 12 {
                                    println!("Found");
                                    let rel_pos = subtract(&point1, point2);
                                    let rel_scanner0: Vec<[i32; 3]> = intersection_set.iter().map(|a| add(a, point1)).collect();
                                    beacons.push(rel_scanner0.clone());
                                    queue.push_back(rel_scanner0.clone());
                                    scanners.push(rel_pos);
                                    not_visited.remove(&i);
                                    //connections.push((scanner0_id, scanner1_id, rel_pos, s, rot));
                                    /*
                                    //dbg!(rel_scanner0);
                                    for point in set2.iter().map(|p| add(&p, point1)) {
                                    ready_points.insert(point);
                                }
                                     */
                                    break 'outer;
                                }
                            }
                        }

                    }

                }
            }
        }
        */


        for comb in data.iter().enumerate().permutations(2) {
            let (scanner0_id, scanner0) = comb[0];
            let (scanner1_id, scanner1) = comb[1];

            //dbg!(scanner0_id, scanner1_id);
            'outer: for rot in 0..24 {
                let s = 0;
                //for rot in 0..6 {
                    let mut transformed: Vec<[i32; 3]> = scanner1.iter().map(|p| gen_rotation(p, s, rot)).collect();

                    for i in 0..transformed.len() {
                        let point2 = transformed[i].clone();
                        for j in 0..scanner0.len() {
                            let point1 = scanner0[j].clone();


                            let target_relative = relative_positions(&transformed, point2);
                            let src_vec: Vec<_> = scanner0.iter().map(|x|*x).collect();
                            let source_relative = relative_positions(&src_vec, point1);

                            let set1: HashSet<[i32; 3]> = HashSet::from_iter(source_relative);
                            let set2: HashSet<[i32; 3]> = HashSet::from_iter(target_relative);
                            let intersection = set1.intersection(&set2);
                            let intersection_set: HashSet<&[i32; 3]> = intersection.collect();
                            if intersection_set.len() >= 12 {
                                //println!("Found");
                                let rel_pos = subtract(&point1, point2);
                                let conn = (scanner1_id, scanner0_id, rel_pos, s, rot);
                                //dbg!(&conn);
                                connections.push(conn);
                                /*
                                let rel_scanner0: Vec<[i32; 3]> = intersection_set.iter().map(|a| add(a, point1)).collect();
                                //dbg!(rel_scanner0);
                                for point in set2.iter().map(|p| add(&p, point1)) {
                                    ready_points.insert(point);
                                }
                                */
                                break 'outer;
                            }
                        }
                    }

                //}
            }
        }


        let mut adjacency_matrix = vec![vec![0; data.len()]; data.len()];
        for con in connections.iter() {

            let from = con.0;
            let to = con.1;
            let relative_pos = con.2;
            let sign = con.3;
            let rot = con.4;

            adjacency_matrix[from][to] = 1;
        }
        let mut paths = Vec::new();

        for i in (0..data.len()).rev() {
            paths.push(bfs(&adjacency_matrix, i, 0));
        }

        let mut all_points = Vec::new();
        let mut positions = Vec::new();
        for (idx, path) in paths.iter().rev().enumerate() {
            println!("Path from {} to 0", idx);
            let mut points = data[idx].clone();
            let mut pos = [0, 0, 0];

            for (from, to) in path.iter().rev().tuple_windows() {

                print!("({}, {}), ", from, to);
                let con = connections.iter().find(|(f, t, _, _, _)| f == from && t == to).unwrap();
                //dbg!(con.2);
                points = points.iter().map(|p| gen_rotation(p, con.3, con.4))
                                               .map(|p| add(&p, con.2))
                    .collect();

                let pos_transformed = add(&gen_rotation(&pos, con.3, con.4), con.2);
                pos = pos_transformed;

            }
            println!("\n");

            all_points.push(points);
            positions.push(pos);
            let test = all_points.iter().flatten().copied().collect::<HashSet<_>>();
            dbg!(test.len());
        }
        let p2: i32 = positions.iter().cartesian_product(positions.iter())
                                 .map(|(a,b)| {
                                     let tmp = subtract(a, *b);
                                     tmp.iter().map(|c| c.abs()).sum()
                                 }).max().unwrap();
        dbg!(p2);
        let mut test = all_points.iter().flatten().copied().collect::<HashSet<_>>().iter().copied().collect::<Vec<_>>();
        test.sort_by(sort_triple);
        test.len() as u32
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        todo!()
    }
}

fn bfs(adj: &Vec<Vec<u32>>, v: usize, target: usize) -> Vec<usize> {
    let mut discovered = vec![false; 100];
    let mut queue = vec![vec![v]];

    while !queue.is_empty() {
        let path = queue.pop().unwrap();
        if path[0] == target {
            return path
        }

        for u in 0..adj[path[0]].len() {
            if adj[path[0]][u] == 1 && !discovered[u] {
                // let mut new_path = vec![u].extend(&path);
                discovered[u] = true;
                let mut new_path = vec![u];
                new_path.extend(&path);
                queue.push(new_path);
            }
        }
    }
    return vec![]
}

fn gen_rotations(p: &[i32; 3]) -> Vec<[i32; 3]>{
    let mut res = Vec::new();

    for s in 0..8 {
        for rot in 0..3 {
            res.push(gen_rotation(p, s, rot));
        }
    }
    res
}

fn gen_rotation(p: &[i32; 3], sign: usize, rot: usize) -> [i32; 3] {
    let signs = [
        [1, 1, 1],
        [1, 1, -1],
        [1, -1, 1],
        [1, -1, -1],
        [-1, 1, 1],
        [-1, 1, -1],
        [-1, -1, 1],
        [-1, -1, -1],
    ];
    //let r: Vec<&usize> = [0usize, 1, 2].iter().permutations(3).skip(rot).next().unwrap();
    let rots = [
        // 1-4
        [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],

        ],
        [
            [-1, 0, 0],
            [0, -1, 0],
            [0, 0, 1],

        ],
        [
            [-1, 0, 0],
            [0, 1, 0],
            [0, 0, -1],

        ],
        [
            [1, 0, 0],
            [0, -1, 0],
            [0, 0, -1],

        ],
        // 5-8
        [
            [-1, 0, 0],
            [0, 0, 1],
            [0, 1, 0],

        ],
        [
            [1, 0, 0],
            [0, 0, -1],
            [0, 1, 0],

        ],
        [
            [1, 0, 0],
            [0, 0, 1],
            [0, -1, 0],

        ],
        [
            [-1, 0, 0],
            [0, 0, -1],
            [0, -1, 0],

        ],
        //9-12
        [
            [0, -1, 0],
            [1, 0, 0],
            [0, 0, 1],

        ],
        [
            [0, 1, 0],
            [-1, 0, 0],
            [0, 0, 1],

        ],
        [
            [0, 1, 0],
            [1, 0, 0],
            [0, 0, -1],

        ],
        [
            [0, -1, 0],
            [-1, 0, 0],
            [0, 0, -1],

        ],
        // 13-16
        [
            [0, 1, 0],
            [0, 0, 1],
            [1, 0, 0],

        ],
        [
            [0, -1, 0],
            [0, 0, -1],
            [1, 0, 0],

        ],
        [
            [0, -1, 0],
            [0, 0, 1],
            [-1, 0, 0],

        ],
        [
            [0, 1, 0],
            [0, 0, -1],
            [-1, 0, 0],

        ],
        // 17-20
        [
            [0, 0, 1],
            [1, 0, 0],
            [0, 1, 0],
        ],
        [
            [0, 0, -1],
            [-1, 0, 0],
            [0, 1, 0],
        ],
        [
            [0, 0, -1],
            [1, 0, 0],
            [0, -1, 0],
        ],
        [
            [0, 0, 1],
            [-1, 0, 0],
            [0, -1, 0],
        ],
        // 21-24
        [
            [0, 0, -1],
            [0, 1, 0],
            [1, 0, 0],
        ],
        [
            [0, 0, 1],
            [0, -1, 0],
            [1, 0, 0],
        ],
        [
            [0, 0, 1],
            [0, 1, 0],
            [-1, 0, 0],
        ],
        [
            [0, 0, -1],
            [0, -1, 0],
            [-1, 0, 0],
        ],
    ];
    let s = signs[sign];
    [
        rots[rot][0][0] * p[0] + rots[rot][0][1] * p[1] + rots[rot][0][2] * p[2],
        rots[rot][1][0] * p[0] + rots[rot][1][1] * p[1] + rots[rot][1][2] * p[2],
        rots[rot][2][0] * p[0] + rots[rot][2][1] * p[1] + rots[rot][2][2] * p[2],
    ]
}

use std::iter::Iterator;
fn relative_positions(points: &Vec<[i32; 3]>, p: [i32; 3]) -> impl Iterator<Item=[i32;3]> + '_ {
    points.iter().map(move |point| {
         [
            point[0] - p[0],
            point[1] - p[1],
            point[2] - p[2],
        ]

    })
}

fn add(a: &[i32; 3], b: [i32; 3]) -> [i32; 3] {
    [
        a[0] + b[0],
        a[1] + b[1],
        a[2] + b[2],
    ]
}
fn subtract(a: &[i32; 3], b: [i32; 3]) -> [i32; 3] {
    [
        a[0] - b[0],
        a[1] - b[1],
        a[2] - b[2],
    ]
}

#[cfg(test)]
mod tests {
    use super::*;
/*
    #[test]
    fn d19p1() {
        assert_eq!(Day19::part1(Day19::parse("--- scanner 0 ---
-1,-1,1
-2,-2,2
-3,-3,3
-2,-3,1
5,6,-4
8,0,7

--- scanner 0 ---
1,-1,1
2,-2,2
3,-3,3
2,-1,3
-5,4,-6
-8,-7,0
")), 0);
    }

*/
    #[test]
    fn d19p1_1() {
        assert_eq!(Day19::part1(Day19::parse("--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14")), 79);

    }
    #[test]
    fn d19p2() {
        assert_eq!(Day19::part2(Day19::parse("")), 0);
    }
}
