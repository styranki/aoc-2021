use crate::{solver::Solver, util::*};

pub struct Day2;

#[derive(Clone, Copy)]
pub enum Direction {
    Forward,
    Down,
    Up,
}

impl<'a> Solver<'a> for Day2 {
    type Parsed = Vec<(Direction, i32)>;
    type Output = i32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|line| {
                let parts: Vec<&str> = line.split(" ").collect();
                let num = parts[1].parse::<i32>().expect("Failed to parse u32");
                let dir = match parts[0] {
                    "forward" => Direction::Forward,
                    "up" => Direction::Up,
                    "down" => Direction::Down,
                    _ => panic!("Invalid direction"),
                };
                (dir, num)
            })
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut initial_coordinates = (0, 0);

        data.iter().for_each(|(d, v)| match d {
            &Direction::Forward => {
                initial_coordinates = (initial_coordinates.0 + v, initial_coordinates.1)
            }
            &Direction::Up => {
                initial_coordinates = (initial_coordinates.0, initial_coordinates.1 - v)
            }
            &Direction::Down => {
                initial_coordinates = (initial_coordinates.0, initial_coordinates.1 + v)
            }
        });
        initial_coordinates.0 * initial_coordinates.1
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut initial_coordinates = (0, 0);
        let mut aim = 0;

        data.iter().for_each(|(d, v)| match d {
            &Direction::Forward => {
                initial_coordinates = (initial_coordinates.0 + v, initial_coordinates.1 - (v * aim))
            }
            &Direction::Up => aim = aim + v,
            &Direction::Down => aim = aim - v,
        });
        initial_coordinates.0 * initial_coordinates.1
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d2p1() {
        assert_eq!(Day2::part1(Day2::parse("")), 0);
    }

    #[test]
    fn d2p2() {
        assert_eq!(Day2::part2(Day2::parse("")), 0);
    }
}
