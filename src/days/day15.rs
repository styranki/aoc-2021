use crate::{solver::Solver, util::*};
use std::cmp::Ordering;
use std::collections::BinaryHeap;

pub struct Day15;

#[derive(Copy, Clone, Eq, PartialEq)]
pub struct State {
    cost: u32,
    position: (usize, usize),
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Solver<'a> for Day15 {
    type Parsed = Vec<Vec<u32>>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|l| l.chars().map(|c| c as u32 - '0' as u32).collect())
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut heap = BinaryHeap::new();
        let width = data[0].len();
        let height = data.len();
        let mut dist = vec![u32::MAX; width * height];
        let mut res = 0;
        heap.push(State {
            cost: 0,
            position: (0, 0),
        });

        while let Some(State { cost, position }) = heap.pop() {
            if position == (width - 1, height - 1) {
                println!("path found");
                res = cost;
            }
            if cost > dist[position.0 + position.1 * width] {
                continue;
            }

            for neigh in gen_neighbours(position, width, height) {
                let next = State {
                    cost: cost + data[neigh.1][neigh.0],
                    position: neigh,
                };

                if next.cost < dist[neigh.0 + neigh.1 * width] {
                    heap.push(next);
                    dist[neigh.0 + neigh.1 * width] = next.cost;
                }
            }
        }
        res
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let width = data[0].len();
        let height = data.len();
        let mut map = vec![vec![0u32; width * 5]; height * 5];
        for j in 0usize..5 {
            for i in 0usize..5 {
                let mut res: Vec<Vec<u32>> = data
                    .iter()
                    .map(|row| {
                        row.iter()
                            .map(|x| {
                                if (x + i as u32 + j as u32) % 9 == 0 {
                                    9
                                } else {
                                    (x + i as u32 + j as u32) % 9
                                }
                            })
                            .collect()
                    })
                    .collect();
                for idx_i in 0..width {
                    //i as usize *width..((i as usize +1)*width) {
                    for idx_j in 0..height {
                        //j as usize *height..((j as usize +1)*height) {
                        map[idx_j + j * height][idx_i + i * width] =
                            res[idx_j as usize][idx_i as usize];
                    }
                }
            }
        }

        let mut heap = BinaryHeap::new();
        let width = data[0].len() * 5;
        let height = data.len() * 5;
        let mut dist = vec![u32::MAX; width * height];
        let mut res = 0;
        heap.push(State {
            cost: 0,
            position: (0, 0),
        });

        while let Some(State { cost, position }) = heap.pop() {
            if position == (width - 1, height - 1) {
                println!("path found");
                res = cost;
            }
            if cost > dist[position.0 + position.1 * width] {
                continue;
            }

            for neigh in gen_neighbours(position, width, height) {
                let next = State {
                    cost: cost + map[neigh.1][neigh.0],
                    position: neigh,
                };

                if next.cost < dist[neigh.0 + neigh.1 * width] {
                    heap.push(next);
                    dist[neigh.0 + neigh.1 * width] = next.cost;
                }
            }
        }
        res
    }
}

use std::iter::Iterator;
fn gen_neighbours(pos: (usize, usize), width: usize, height: usize) -> Vec<(usize, usize)> {
    let mut res = vec![];
    for i in pos.0.saturating_sub(1)..(pos.0 + 2).min(width) {
        for j in pos.1.saturating_sub(1)..(pos.1 + 2).min(height) {
            if i != pos.0 && j != pos.1 {
                continue;
            }
            if i == pos.0 && j == pos.1 {
                continue;
            }
            res.push((i, j));
        }
    }
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d15p1() {
        assert_eq!(
            Day15::part1(Day15::parse(
                "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"
            )),
            40
        );
    }

    #[test]
    fn d15p2() {
        assert_eq!(
            Day15::part2(Day15::parse(
                "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"
            )),
            315
        );
    }
}
