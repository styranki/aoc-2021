use crate::{solver::Solver, util::*};
use itertools::Itertools;
use std::collections::HashSet;

pub struct Day13;

impl<'a> Solver<'a> for Day13 {
    type Parsed = (Vec<(i32, i32)>, Vec<(i32, i32)>);
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        let (dots, folds) = input.split_once("\n\n").unwrap();

        let points = dots
            .lines()
            .map(|l| {
                l.split_once(",")
                    .map(|x| (x.0.parse::<i32>().unwrap(), x.1.parse::<i32>().unwrap()))
            })
            .map(Option::unwrap)
            .collect();
        let folds = folds
            .lines()
            .map(|l| {
                l.split_once("fold along ").map(|x| {
                    if &(x.1)[0..1] == "x" {
                        (x.1[2..].parse::<i32>().unwrap(), 0)
                    } else {
                        (0, x.1[2..].parse::<i32>().unwrap())
                    }
                })
            })
            .map(Option::unwrap)
            .collect();

        return (points, folds);
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut points = data.0.clone();
        //dbg!(points.len());
        for fold in data.1[0..1].iter() {
            //dbg!(&fold);
            match fold {
                (x, 0) => {
                    for p in points.iter_mut() {
                        *p = ((p.0 - 2 * (p.0 - x).max(0)), p.1);
                    }
                }
                (0, y) => {
                    for p in points.iter_mut() {
                        *p = (p.0, (p.1 - 2 * (p.1 - y).max(0)));
                    }
                }
                _ => panic!("invalid fold"),
            }
        }
        use std::collections::HashSet;
        let mut res = HashSet::new();
        for p in points {
            res.insert(p);
        }
        res.len() as u32
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut points = data.0.clone();
        //dbg!(points.len());
        for fold in data.1.iter() {
            //dbg!(&fold);
            match fold {
                (x, 0) => {
                    for p in points.iter_mut() {
                        *p = ((p.0 - 2 * (p.0 - x).max(0)), p.1);
                    }
                }
                (0, y) => {
                    for p in points.iter_mut() {
                        *p = (p.0, (p.1 - 2 * (p.1 - y).max(0)));
                    }
                }
                _ => panic!("invalid fold"),
            }
        }
        let mut res = HashSet::new();
        for p in points {
            res.insert(p);
        }
        // Results visualization
        //visualize(&res);
        res.len() as u32
    }
}

fn visualize(points: &HashSet<(i32, i32)>) {
    let max_x = points.iter().max_by(|x, y| x.0.cmp(&y.0)).unwrap();
    let max_y = points.iter().max_by(|x, y| x.1.cmp(&y.1)).unwrap();
    println!("");
    for j in 0..=max_y.1 {
        let mut row = String::new();
        for i in 0..=max_x.0 {
            if points.get(&(i, j)).is_some() {
                row.push('#');
            } else {
                row.push('.');
            }
        }
        println!("{}", row);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d13p1() {
        assert_eq!(
            Day13::part1(Day13::parse(
                "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"
            )),
            17
        );
    }

    #[test]
    fn d13p2() {
        assert_eq!(
            Day13::part2(Day13::parse(
                "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"
            )),
            16
        );
    }
}
