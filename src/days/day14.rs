use crate::{solver::Solver, util::*};
use itertools::Itertools;
use std::collections::HashMap;

pub struct Day14;

impl<'a> Solver<'a> for Day14 {
    type Parsed = (String, HashMap<&'a str, char>);
    type Output = u64;

    fn parse(input: &'a str) -> Self::Parsed {
        let (template, rules) = input.split_once("\n\n").unwrap();
        let mut out = HashMap::new();
        for r in rules.lines() {
            let (pair, insert) = r.split_once(" -> ").unwrap();
            out.insert(pair.into(), insert.chars().next().unwrap());
        }

        (template.to_string(), out)
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut out = data.0.clone();

        for i in 0..10 {
            let mut step: String = out
                .chars()
                .tuple_windows()
                .map(|(a, b)| {
                    let insertion = data.1.get(format!("{}{}", a, b).as_str()).unwrap();
                    format!("{}{}", a, insertion)
                })
                .collect();
            step.push(out.chars().last().unwrap());
            //dbg!(&step);
            out = step;
        }

        let unique: Vec<char> = out.chars().unique().collect();
        let mut unique_counts: Vec<(&char, usize)> = unique
            .iter()
            .map(|c| (c, out.chars().filter(|o| o == c).count()))
            .collect();
        let mut sorted = unique_counts
            .iter_mut()
            .sorted_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap());
        let first = sorted.next().unwrap();
        let last = sorted.last().unwrap();

        (last.1 - first.1) as u64
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut polymer_counts: HashMap<(char, char), usize> = HashMap::new();
        for p in data.0.chars().tuple_windows::<(char, char)>() {
            *polymer_counts.entry((p.0, p.1)).or_default() += 1;
        }
        //dbg!(&polymer_counts);

        for i in 0..40 {
            let mut new_polymers: HashMap<(char, char), usize> = HashMap::new();
            for (chars, count) in polymer_counts.iter() {
                let insertion = data
                    .1
                    .get(format!("{}{}", chars.0, chars.1).as_str())
                    .unwrap();
                *new_polymers.entry((chars.0, *insertion)).or_default() += count;
                *new_polymers.entry((*insertion, chars.1)).or_default() += count;
            }
            polymer_counts = new_polymers;
        }

        let mut letter_counts: HashMap<char, usize> = HashMap::new();
        for (p, count) in polymer_counts.iter() {
            *letter_counts.entry(p.0).or_default() += count;
            *letter_counts.entry(p.1).or_default() += count;
        }

        *letter_counts
            .entry(data.0.chars().next().unwrap())
            .or_default() += 1;
        *letter_counts
            .entry(data.0.chars().last().unwrap())
            .or_default() += 1;

        for (p, count) in letter_counts.clone().iter() {
            *letter_counts.entry(*p).or_default() = *count / 2;
        }
        let mut sort = letter_counts
            .iter()
            .sorted_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap());
        let first = sort.next().unwrap().1;
        let last = sort.last().unwrap().1;
        (last - first) as u64
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d14p1() {
        assert_eq!(
            Day14::part1(Day14::parse(
                "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"
            )),
            1588
        );
    }

    #[test]
    fn d14p2() {
        assert_eq!(
            Day14::part2(Day14::parse(
                "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"
            )),
            1588
        );
    }
}
