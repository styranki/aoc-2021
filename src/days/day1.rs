use crate::{solver::Solver, util::*};

pub struct Day1;

impl<'a> Solver<'a> for Day1 {
    type Parsed = Vec<u32>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .lines()
            .map(|line| line.parse::<u32>().expect("Failed to parse u32"))
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        data.windows(2).fold(
            0u32,
            |acc, elem| {
                if elem[1] > elem[0] {
                    acc + 1
                } else {
                    acc
                }
            },
        )
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        data.windows(4).fold(0u32, |acc, elem| {
            //dbg!(&elem[1..4].iter().sum::<u32>(), elem[0..3].iter().sum::<u32>() < elem[1..4].iter().sum::<u32>());
            if elem[0..3].iter().sum::<u32>() < elem[1..4].iter().sum::<u32>() {
                acc + 1
            } else {
                acc
            }
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d1p1() {
        assert_eq!(Day1::part1(Day1::parse("")), 0);
    }

    #[test]
    fn d1p2() {
        assert_eq!(
            Day1::part2(Day1::parse(
                "199
200
208
210
200
207
240
269
260
263"
            )),
            5
        );
    }
}
