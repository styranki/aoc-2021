use crate::{solver::Solver, util::*};
use itertools::sorted;

pub struct Day7;

impl<'a> Solver<'a> for Day7 {
    type Parsed = Vec<i32>;
    type Output = i32;

    fn parse(input: &'a str) -> Self::Parsed {
        input
            .split(",")
            .map(|x| x.parse::<i32>().unwrap())
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let median = sorted(data.iter()).nth(data.len() / 2).unwrap();

        data.iter().fold(0, |acc, val| acc + (val - median).abs())
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mean: f32 = data.iter().sum::<i32>() as f32 / data.len() as f32;
        let mean_min = mean.floor() as i32;
        let mean_max = mean.ceil() as i32;

        let sum_min = data.iter().fold(0, |acc, val| {
            acc + ((val - mean_min).abs() as f32 * (((val - mean_min).abs() + 1) as f32 / 2.0))
                as i32
        });
        let sum_max = data.iter().fold(0, |acc, val| {
            acc + ((val - mean_max).abs() as f32 * (((val - mean_max).abs() + 1) as f32 / 2.0))
                as i32
        });

        sum_min.min(sum_max)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d7p1() {
        assert_eq!(Day7::part1(Day7::parse("16,1,2,0,4,2,7,1,2,14")), 37);
    }

    #[test]
    fn d7p2() {
        assert_eq!(Day7::part2(Day7::parse("16,1,2,0,4,2,7,1,2,14")), 168);
    }
}
