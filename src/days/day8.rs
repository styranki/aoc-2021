use itertools::Itertools;
use std::collections::HashSet;

use crate::{solver::Solver, util::*};

pub struct Day8;

impl<'a> Solver<'a> for Day8 {
    type Parsed = Vec<(Vec<&'a str>, Vec<&'a str>)>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        let lines: Vec<&str> = input.lines().collect();
        lines
            .iter()
            .map(|x| x.split(" | "))
            .map(|l| {
                let tmp: Vec<Vec<&str>> = l.map(|p| p.split(" ").collect()).collect();
                (tmp[0].clone(), tmp[1].clone())
            })
            .collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut out = 0;
        for d in data.iter() {
            let output = &d.1;
            let output_sorted: Vec<usize> = output
                .iter()
                //.sorted_by(|x, y| x.len().partial_cmp(&y.len()).unwrap())
                .map(|&x| x.len())
                .sorted()
                .collect();

            let one = output_sorted.iter().filter(|&&x| x == 2).count();
            let seven = output_sorted.iter().filter(|&&x| x == 3).count();
            let four = output_sorted.iter().filter(|&&x| x == 4).count();
            let eight = output_sorted.iter().filter(|&&x| x == 7).count();

            out += (one + seven + four + eight);

            /*
             */
        }
        out as u32
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut sum = 0;
        for d in data.iter() {
            let digits = &d.0;
            let digits_sorted: Vec<&str> = digits
                .iter()
                .sorted_by(|x, y| x.len().partial_cmp(&y.len()).unwrap())
                .map(|&x| x)
                .collect();

            let output = &d.1;
            let one = string_to_hashset(digits_sorted[0]);
            let four = string_to_hashset(digits_sorted[2]);
            let seven = string_to_hashset(digits_sorted[1]);
            let eight = string_to_hashset(digits_sorted[9]);
            let tmp: HashSet<char> = string_to_hashset(digits_sorted[7])
                .intersection(&string_to_hashset(digits_sorted[8]))
                .map(|&x| x)
                .collect();
            let zero_six_nine = string_to_hashset(digits_sorted[6])
                .intersection(&tmp)
                .map(|&x| x)
                .collect();

            let wires: HashSet<char> = HashSet::from(['a', 'b', 'c', 'd', 'e', 'f', 'g']);
            let mut result = vec![wires.clone(); 7];
            // c
            result[2] = result[2].intersection(&one).map(|&x| x).collect();
            // f
            result[5] = result[5].intersection(&one).map(|&x| x).collect();

            result[0] = result[0].difference(&one).map(|&x| x).collect();
            result[1] = result[1].difference(&one).map(|&x| x).collect();
            result[3] = result[3].difference(&one).map(|&x| x).collect();
            result[4] = result[4].difference(&one).map(|&x| x).collect();
            result[6] = result[6].difference(&one).map(|&x| x).collect();

            // 0
            result[0] = result[0]
                .intersection(&seven.difference(&one).map(|&x| x).collect())
                .map(|&x| x)
                .collect();
            for i in (1..7) {
                result[i] = result[i].difference(&result[0]).map(|&x| x).collect();
            }
            // 0, 2
            result[2] = result[2].difference(&zero_six_nine).map(|&x| x).collect();
            // 0, 2, 5
            result[5] = result[5].difference(&result[2]).map(|&x| x).collect();

            // b
            result[1] = result[1].intersection(&four).map(|&x| x).collect();
            result[4] = result[4].difference(&four).map(|&x| x).collect();

            result[3] = result[3].difference(&zero_six_nine).map(|&x| x).collect();

            result[6] = result[4].clone();
            result[4] = result[4].difference(&zero_six_nine).map(|&x| x).collect();
            result[6] = result[6].difference(&result[4]).map(|&x| x).collect();

            result[3] = result[3].difference(&result[4]).map(|&x| x).collect();
            result[1] = result[1].difference(&result[3]).map(|&x| x).collect();

            let mut key = Vec::new();
            for i in 0..7 {
                key.push(*result[i].iter().collect::<Vec<&char>>()[0]);
            }
            let tmp: Vec<(usize, &char)> = key.iter().enumerate().collect();
            let tmp: Vec<(usize, &char)> = tmp
                .iter()
                .sorted_by(|(_, k), (_, k2)| k.partial_cmp(k2).unwrap())
                .map(|&x| x)
                .collect();

            let mut res = 0;
            for (i, number) in output.iter().rev().enumerate() {
                let decoded = decode_string(number, &tmp);
                //dbg!(number);
                //dbg!(&tmp);
                //dbg!(&decoded);
                res = res
                    + 10_i32.pow(i as u32)
                        * match decoded.as_str() {
                            "abcefg" => 0,
                            "cf" => 1,
                            "acdeg" => 2,
                            "acdfg" => 3,
                            "bcdf" => 4,
                            "abdfg" => 5,
                            "abdefg" => 6,
                            "acf" => 7,
                            "abcdefg" => 8,
                            "abcdfg" => 9,
                            _ => panic!("String decode failed"),
                        };
            }
            sum += res;
        }
        sum as u32
    }
}

fn string_to_hashset(s: &str) -> HashSet<char> {
    let mut res = HashSet::new();
    s.chars().for_each(|c| {
        res.insert(c);
    });
    res
}

fn decode_string<'a>(s: &'a str, key: &[(usize, &char)]) -> String {
    let out = s
        .chars()
        .map(|c| unsafe { std::char::from_u32_unchecked(key[(c as usize - 97)].0 as u32 + 97) })
        .sorted()
        .collect();
    out
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d8p1() {
        assert_eq!(Day8::part1(Day8::parse("")), 0);
    }

    #[test]
    fn d8p2() {
        assert_eq!(Day8::part2(Day8::parse("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")), 5353);
    }
}
