use crate::{solver::Solver, util::*};

pub struct Day10;

impl<'a> Solver<'a> for Day10 {
    type Parsed = Vec<&'a str>;
    type Output = u64;

    fn parse(input: &'a str) -> Self::Parsed {
        input.lines().collect()
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        data.iter().fold(0, |acc, line| {
            use std::collections::VecDeque;
            let mut stack = VecDeque::new();
            for (i, c) in line.chars().enumerate() {
                match c {
                    '(' => stack.push_front('('),
                    '[' => stack.push_front('['),
                    '{' => stack.push_front('{'),
                    '<' => stack.push_front('<'),
                    ')' => {
                        let front = stack.pop_front();
                        if front.is_none() || front.unwrap() != '(' {
                            return acc + 3;
                        }
                    }
                    ']' => {
                        let front = stack.pop_front();
                        if front.is_none() || front.unwrap() != '[' {
                            return acc + 57;
                        }
                    }
                    '}' => {
                        let front = stack.pop_front();
                        if front.is_none() || front.unwrap() != '{' {
                            return acc + 1197;
                        }
                    }
                    '>' => {
                        let front = stack.pop_front();
                        if front.is_none() || front.unwrap() != '<' {
                            return acc + 25137;
                        }
                    }
                    _ => {
                        panic!("Invalid character");
                    }
                }
            }
            return acc;
        })
    }
    fn part2(data: Self::Parsed) -> Self::Output {
        let scores: Vec<u64> = data
            .iter()
            .map(|line| {
                use std::collections::VecDeque;
                let mut stack = VecDeque::new();
                for (i, c) in line.chars().enumerate() {
                    match c {
                        '(' => stack.push_front('('),
                        '[' => stack.push_front('['),
                        '{' => stack.push_front('{'),
                        '<' => stack.push_front('<'),
                        ')' => {
                            let front = stack.pop_front();
                            if front.is_none() || front.unwrap() != '(' {
                                return 0;
                            }
                        }
                        ']' => {
                            let front = stack.pop_front();
                            if front.is_none() || front.unwrap() != '[' {
                                return 0;
                            }
                        }
                        '}' => {
                            let front = stack.pop_front();
                            if front.is_none() || front.unwrap() != '{' {
                                return 0;
                            }
                        }
                        '>' => {
                            let front = stack.pop_front();
                            if front.is_none() || front.unwrap() != '<' {
                                return 0;
                            }
                        }
                        _ => {
                            panic!("Invalid character");
                        }
                    }
                }

                let mut sum: u64 = 0;
                while let Some(c) = stack.pop_front() {
                    sum = match c {
                        '(' => sum * 5 + 1,
                        '[' => sum * 5 + 2,
                        '{' => sum * 5 + 3,
                        '<' => sum * 5 + 4,
                        _ => panic!("Invalid char"),
                    }
                }
                sum
            })
            .collect();

        let mut incomplete = scores
            .iter()
            .filter(|&&s| s != 0)
            .map(|&x| x)
            .collect::<Vec<u64>>();
        incomplete.sort();
        incomplete[incomplete.len() / 2]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d10p1() {
        assert_eq!(
            Day10::part1(Day10::parse(
                "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"
            )),
            26397
        );
    }

    #[test]
    fn d10p2() {
        assert_eq!(
            Day10::part2(Day10::parse(
                "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"
            )),
            288957
        );
    }
}
