use crate::{solver::Solver, util::*};

pub struct Day9;

impl<'a> Solver<'a> for Day9 {
    type Parsed = Vec<Vec<u32>>;
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        let mut tmp: Vec<Vec<u32>> = input
            .lines()
            .map(|l| {
                let mut res: Vec<u32> = vec![10];
                let mut nums = l.chars().map(|x| x.to_digit(10).unwrap()).collect();
                res.append(&mut nums);
                res.push(10);
                res
            })
            .collect();
        let len = tmp[0].len();
        let mut res = vec![vec![10; len]];
        res.append(&mut tmp);
        res.push(vec![10; len]);
        res
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let height = data.len();
        let width = data[0].len();

        let mut risk_sum = 0;
        for y in 1..(height - 1) {
            for x in 1..(width - 1) {
                let p = data[y][x];
                if p < data[y - 1][x]
                    && p < data[y + 1][x]
                    && p < data[y][x - 1]
                    && p < data[y][x + 1]
                {
                    risk_sum += (1 + p);
                }
            }
        }
        risk_sum
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let height = data.len();
        let width = data[0].len();

        let mut basins: Vec<HashSet<(usize, usize)>> = Vec::new();
        for y in 1..(height - 1) {
            for x in 1..(width - 1) {
                let p = data[y][x];
                if p < data[y - 1][x]
                    && p < data[y + 1][x]
                    && p < data[y][x - 1]
                    && p < data[y][x + 1]
                {
                    let mut points = HashSet::new();
                    points.insert((x, y));
                    flood_fill(&data, (x, y), &mut points);
                    basins.push(points);
                }
            }
        }
        let mut test: Vec<(usize, usize)> = basins
            .iter()
            .enumerate()
            .map(|(i, b)| (i, b.len()))
            .collect();
        test.sort_by(|(_, s), (_, s2)| s2.partial_cmp(s).unwrap());
        test.iter().take(3).fold(1, |acc, val| acc * val.1 as u32)
    }
}
use std::collections::HashSet;
fn flood_fill(map: &Vec<Vec<u32>>, point: (usize, usize), points: &mut HashSet<(usize, usize)>) {
    let p = map[point.1][point.0];
    let up = map[point.1 - 1][point.0];
    let down = map[point.1 + 1][point.0];
    let left = map[point.1][point.0 - 1];
    let right = map[point.1][point.0 + 1];

    if up >= p && up < 9 {
        if points.insert((point.0, point.1 - 1)) {
            flood_fill(map, (point.0, point.1 - 1), points);
        }
    }
    if down >= p && down < 9 {
        if points.insert((point.0, point.1 + 1)) {
            flood_fill(map, (point.0, point.1 + 1), points);
        }
    }

    if left >= p && left < 9 {
        if points.insert((point.0 - 1, point.1)) {
            flood_fill(map, (point.0 - 1, point.1), points);
        }
    }
    if right >= p && right < 9 {
        if points.insert((point.0 + 1, point.1)) {
            flood_fill(map, (point.0 + 1, point.1), points);
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d9p1() {
        assert_eq!(
            Day9::part1(Day9::parse(
                "2199943210
3987894921
9856789892
8767896789
9899965678"
            )),
            15
        );
    }

    #[test]
    fn d9p2() {
        assert_eq!(
            Day9::part2(Day9::parse(
                "2199943210
3987894921
9856789892
8767896789
9899965678"
            )),
            1134
        );
    }
}
