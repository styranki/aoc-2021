use crate::{solver::Solver, util::*};
use std::collections::HashMap;

pub struct Day21;

impl<'a> Solver<'a> for Day21 {
    type Parsed = (u32, u32);
    type Output = u64;

    fn parse(input: &'a str) -> Self::Parsed {
        let mut player_positions = input
            .lines()
            .map(|l| l.split_once(": ").unwrap().1.parse::<u32>().unwrap());

        (
            player_positions.next().unwrap(),
            player_positions.next().unwrap(),
        )
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut pos_1 = data.0;
        let mut pos_2 = data.1;

        let mut score_1 = 0;
        let mut score_2 = 0;

        let mut dice = 0;
        let mut turn = 0;

        'outer: loop {
            if turn % 2 == 0 {
                for i in 0..3 {
                    pos_1 = (pos_1 + dice) % 10 + 1;
                    dice = dice % 100 + 1;
                }
                score_1 += pos_1;
                if score_1 >= 1000 {
                    break 'outer;
                }
            } else {
                for i in 0..3 {
                    pos_2 = (pos_2 + dice) % 10 + 1;
                    dice = dice % 100 + 1;
                }
                score_2 += pos_2;
                if score_2 >= 1000 {
                    break 'outer;
                }
            }
            turn += 1;
        }

        ((1 + turn) * 3 * score_1.min(score_2)) as u64
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut pos_1 = data.0;
        let mut pos_2 = data.1;

        let mut roll_counts: HashMap<u64, u64> = HashMap::new();
        for d1 in 1..4 {
            for d2 in 1..4 {
                for d3 in 1..4 {
                   let sum = d1 + d2 + d3;
                    *roll_counts.entry(sum).or_default() += 1;
                }
            }
        }
        let mut game_states: HashMap<((u64, u64), (u64, u64), u64), u64> = HashMap::new();
        game_states.insert(((data.0.into(), data.1.into()), (0, 0), 0), 1);
        let mut wins: (u64, u64) = (0, 0);

        while game_states.len() > 0 {
            let key = game_states.keys().nth(0).copied().unwrap();
            let times = game_states.remove(&key).unwrap();
            let (pos, scores, turn) = key;

            for (roll, n) in roll_counts.iter() {
                let new_count = times * n;
                let new_positions = if turn == 0 {
                    ((pos.0 + roll - 1) % 10 + 1, pos.1)
                } else {
                    (pos.0, (pos.1 + roll - 1) % 10 + 1)
                };
                let new_turn = 1 - turn;
                let new_scores = if turn == 0 {
                    (scores.0 + new_positions.0, scores.1)
                } else {
                    (scores.0, scores.1 + new_positions.1)
                };
                if new_scores.0 >= 21 {
                    wins.0 += new_count;
                } else if new_scores.1 >= 21 {
                    wins.1 += new_count;
                } else {
                    *game_states.entry((new_positions, new_scores, new_turn)).or_default() += new_count;
                }
            }
            //dbg!(&game_states);
            //break;
        }

        wins.0.max(wins.1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d21p1() {
        assert_eq!(
            Day21::part1(Day21::parse("Player 1 starting position: 4
Player 2 starting position: 8"
            )),
            739785
        );
    }

    #[test]
    fn d21p2() {
        assert_eq!(Day21::part2(Day21::parse("Player 1 starting position: 4
Player 2 starting position: 8"
)), 444356092776315);
    }
}
