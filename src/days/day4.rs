use crate::{solver::Solver, util::*};

pub struct Day4;

impl<'a> Solver<'a> for Day4 {
    type Parsed = (Vec<u32>, Vec<Vec<u32>>);
    type Output = u32;

    fn parse(input: &'a str) -> Self::Parsed {
        let lines: Vec<&str> = input.lines().collect();
        let numbers = lines[0]
            .split(",")
            .map(|x| x.parse::<u32>().unwrap())
            .collect();

        let mut boards: Vec<Vec<u32>> = Vec::new();
        for j in (2..lines.len()).step_by(6) {
            let mut b: Vec<u32> = Vec::new();
            for i in 0..5 {
                let line: &str = lines[j + i];
                let mut nums: Vec<u32> = line
                    .split_ascii_whitespace()
                    .map(|x| x.parse::<u32>().unwrap())
                    .collect();
                b.append(&mut nums);
            }
            boards.push(b);
        }
        (numbers, boards)
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        for j in (0..data.0.len()) {
            //println!("Number: {}", j);
            for i in (0..data.1.len()) {
                if board_is_winner(&data.1[i], &data.0[0..j]) {
                    let unmarked_numbers: Vec<&u32> = data.1[i]
                        .iter()
                        .filter(|&x| data.0[0..j].iter().all(|y| y != x))
                        .collect();
                    let unmarked_sum: u32 = unmarked_numbers.iter().fold(0, |acc, val| acc + **val);
                    //dbg!(unmarked_numbers);
                    return unmarked_sum * data.0[j - 1];
                }
            }
        }

        0
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut no_win_boards = std::collections::HashSet::new();
        for i in (0..data.1.len()) {
            no_win_boards.insert(i);
        }
        for j in (0..data.0.len()) {
            //println!("Number: {}", j);
            for i in (0..data.1.len()) {
                if board_is_winner(&data.1[i], &data.0[0..j]) {
                    if no_win_boards.take(&i).is_some() {}
                    if no_win_boards.len() == 0 {
                        let unmarked_numbers: Vec<&u32> = data.1[i]
                            .iter()
                            .filter(|&x| data.0[0..j].iter().all(|y| y != x))
                            .collect();
                        let unmarked_sum: u32 =
                            unmarked_numbers.iter().fold(0, |acc, val| acc + **val);
                        //dbg!(unmarked_numbers);
                        return unmarked_sum * data.0[j - 1];
                    }
                }
            }
        }

        0
    }
}

fn board_is_winner(board: &[u32], numbers: &[u32]) -> bool {
    let winner: bool = false;

    for i in (0..5) {
        // Check for row wins
        if board[5 * i..5 + (i * 5)]
            .iter()
            .filter(|&x| numbers.iter().any(|y| x == y))
            .count()
            == 5
        {
            //println!("Winning row {}", i);
            return true;
        }
        // Check for column wins
        let col: Vec<u32> = (0 + i..21 + i).step_by(5).map(|x| board[x]).collect();
        if col
            .iter()
            .filter(|&x| numbers.iter().any(|y| x == y))
            .count()
            == 5
        {
            // println!("Winning col {}", i);
            return true;
        }
    }
    /*
     * WHOOPS diagonald don't count
    // Diagonal wins
    let diag1 = [board[0], board[6], board[12], board[18], board[24]];
    let diag2 = [board[4], board[8], board[12], board[16], board[20]];

    if diag1.iter().filter(|&x| numbers.iter().any(|y| x == y)).count() == 5 {
        println!("Winning diag 1");
        return true
    }
    if diag2.iter().filter(|&x| numbers.iter().any(|y| x == y)).count() == 5 {
        println!("Winning diag 2");
        return true
    }
    */

    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d4p1() {
        assert_eq!(
            Day4::part1(Day4::parse(
                "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"
            )),
            4512
        );
    }

    #[test]
    fn d4p2() {
        assert_eq!(
            Day4::part2(Day4::parse(
                "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"
            )),
            1924
        );
    }
}
