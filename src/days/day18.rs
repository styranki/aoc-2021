use crate::{solver::Solver, util::*};
use itertools::Itertools;
use std::boxed::Box;

pub struct Day18;
#[derive(Clone, Debug)]
pub enum Elem{
    Pair(Box<Elem>, Box<Elem>),
    Number(u32),
}

impl std::fmt::Display for Elem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Elem::Number(n) => {
                write!(f, "{}", n)
            },
            Elem::Pair(l, r) => {
                write!(f, "[{}, {}]", l, r)
            }
        }
    }
}

impl<'a> Solver<'a> for Day18 {
    type Parsed = Vec<Elem>;
    type Output = u64;

    fn parse(input: &'a str) -> Self::Parsed {
        let mut out = Vec::new();
        for row in input.lines() {


        let mut stack = Vec::new();
        let mut ready_stack = Vec::new();
        for c in row.chars() {
            if c != ']' {
                stack.push(c);
            } else {
                //let second_num = stack.pop().unwrap();
                //let comma = stack.pop().unwrap();
                //let first_num = stack.pop().unwrap();
                let mut stack_str = String::new();
                loop {
                    let tmp = stack.pop().unwrap();
                    if tmp == '[' {
                        break;
                    }
                    stack_str.push(tmp);
                }
                //dbg!(&stack_str);
                if &stack_str == "," {
                    let second = ready_stack.pop().unwrap();
                    let first = ready_stack.pop().unwrap();
                    ready_stack.push(Box::new(Elem::Pair(
                        first,
                        second,
                    )));

                } else if stack_str.len() == 3 {
                    let second_num = stack_str.chars().next().unwrap().to_digit(10);
                    let first_num = stack_str.chars().skip(2).next().unwrap().to_digit(10);

                    ready_stack.push(Box::new(Elem::Pair(
                        Box::new(Elem::Number(first_num.unwrap())),
                        Box::new(Elem::Number(second_num.unwrap())),
                    )));
                } else {
                    let second = stack_str.chars().next().unwrap();
                    let first = stack_str.chars().skip(1).next().unwrap();
                    let ready = ready_stack.pop().unwrap();
                    if first == ',' {
                        ready_stack.push(Box::new(Elem::Pair(
                            ready,
                            Box::new(Elem::Number(second.to_digit(10).unwrap())),
                        )));
                    } else if stack_str.len() == 2 {

                        ready_stack.push(Box::new(Elem::Pair(
                            Box::new(Elem::Number(first.to_digit(10).unwrap())),
                            ready,
                        )));
                    } else {
                        panic!("sth wrong")
                    }
                }
            }
        }
            out.push(*ready_stack.pop().unwrap())
        }
        out
    }

    fn part1(data: Self::Parsed) -> Self::Output {
        let mut first = data[0].clone();
        for elem in data[1..].iter() {
            //println!("{}", &first);
            let second = elem;
            //first = Elem::Pair(second.clone().into(), (first).into());
            first = Elem::Pair(first.into(), second.clone().into());
            reduce(&mut first);

        }
        // println!("############## RESULT ##############");
        // println!("{}", &first);
        magnitude(&first)
    }

    fn part2(data: Self::Parsed) -> Self::Output {
        let mut largest_magnitude = 0;
        for comb in data.iter().combinations(2) {

            let mut pair = Elem::Pair(comb[0].clone().into(), comb[1].clone().into());
            let mut pair_rev = Elem::Pair(comb[1].clone().into(), comb[0].clone().into());
            reduce(&mut pair);
            reduce(&mut pair_rev);
            largest_magnitude = largest_magnitude
                .max(magnitude(&pair))
                .max(magnitude(&pair_rev));
        }
        largest_magnitude
    }
}

fn reduce(elem: &mut Elem) {
    //dbg!(&elem);
    loop {
        let mut exp = None;
        explode(elem, &mut None, &mut exp, 0);
        if exp.is_some() {
            //println!("explode");
            //dbg!(&elem);
            continue; }
        if split(elem) {
            //println!("split");
            //dbg!(&elem);
            continue; }
        break;
    }
}

fn explode<'a>(elem: &'a mut Elem, pn: &mut Option<&'a mut u32>, exp: &mut Option<u32>, depth: usize) -> bool {
    match elem {
        Elem::Number(n) => {
            if let Some(exp) = exp { *n += *exp; return true }
            else { *pn = Some(n) }
        },
        Elem::Pair(box Elem::Number(f), box Elem::Number(s)) if depth >= 4 && exp.is_none() => {
            if let Some(pn) = pn { **pn  += *f;}
            *exp = Some(*s);
            *elem = Elem::Number(0);
        },
        Elem::Pair(f, s) => {
            if explode(f, pn, exp, depth+1) { return true; }
            if explode(s, pn, exp, depth+1) { return true; }
        }
    }
    false
}

fn split(elem: &mut Elem) -> bool {
    match elem {
        Elem::Number(n) if *n >= 10 => {
            *elem = Elem::Pair(Elem::Number(*n/2).into(), Elem::Number((*n+1)/2).into());
            return true;

        },
        Elem::Number(_) => {},
        Elem::Pair(f, s) => {
            if split(f) { return true; }
            if split(s) { return true; }
        }
    }
    false
}

fn magnitude(elem: &Elem) -> u64 {
    match elem {
        Elem::Number(n) => *n as u64,
        Elem::Pair(l, r) => 3 * magnitude(l) + 2 * magnitude(r),

    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d18p1() {
        assert_eq!(Day18::part1(Day18::parse("[[[[4,3],4],4],[7,[[8,4],9]]]
[1,1]")), 1384);
    }

    #[test]
    fn d18p1_1() {
       assert_eq!(Day18::part1(Day18::parse("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]
")), 3488);
    }


    #[test]
    fn d18p2() {
        assert_eq!(Day18::part2(Day18::parse("")), 0);
    }
}
